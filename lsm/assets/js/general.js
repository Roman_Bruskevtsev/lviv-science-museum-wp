'use strict';

class GeneralClass{
    constructor(){
        this.init();
    }

    init(){
        this.documentReady();
        this.windowLoad();
    }

    documentReady(){
        document.addEventListener('DOMContentLoaded', function(){
            AOS.init();
        });
    }
    windowLoad(){
        window.onload = function() {
            
        }
    }
}

let general = new GeneralClass();