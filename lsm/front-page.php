<?php
/**
 * @package WordPress
 * @subpackage LSM
 * @since 1.0
 * @version 1.0
 */

get_header(); 

while ( have_posts() ) : the_post(); 

$background = get_field('background') ? ' style="background-image: url('.get_field('background').')"' : ''; 
$contacts = get_field('contacts', 'option'); ?>

<section class="lsm-temporary__section"<?php echo $background; ?>>
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-xl-10">
                <div class="lsm-temporary__wrapper">
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-lg-5">
                        <?php if( get_field('logo') ) { ?>
                            <div class="lsm-temporary__logo">
                                <img src="<?php echo get_field('logo')['url']; ?>" alt="<?php echo get_field('logo')['title']; ?>">
                            </div>
                        <?php } ?>
                        </div>
                        <div class="col-lg-5">
                            <div class="lsm-temporary__text">
                            <?php if( get_field('title') ) { ?>
                                <h1><?php the_field('title'); ?></h1>
                            <?php }
                            if( get_field('text') ) { ?>
                                <p><?php the_field('text'); ?></p>
                            <?php } ?>
                            </div>
                        </div>
                        <div class="col-lg-1"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-xl-2">
                <div class="lsm-temporary__contacts">
                <?php if( $contacts['address'] ) { ?>
                    <div class="lsm-temporary__address">
                        <h3><?php echo $contacts['address']; ?></h3>
                    </div>
                <?php } 
                if( $contacts['phone'] ) { ?>
                    <div class="lsm-temporary__phone">
                        <h6><?php _e('cooperation and questions', 'lsm'); ?></h6>
                        <a href="tel:<?php echo $contacts['phone']; ?>">
                            <h4><?php echo $contacts['phone']; ?></h4>
                        </a>
                    </div>
                <?php } 
                if( $contacts['gmail'] ) { ?>
                    <div class="lsm-temporary__social gmail">
                        <a class="gmail" href="mailto:<?php echo $contacts['gmail']; ?>">
                            <h4><?php _e('Gmail', 'lsm'); ?></h4>
                        </a>
                    </div>
                <?php }
                if( $contacts['facebook'] ) { ?>
                    <div class="lsm-temporary__social facebook">
                        <a class="facebook" target="_blank" href="<?php echo $contacts['facebook']; ?>">
                            <h4><?php _e('Facebook', 'lsm'); ?></h4>
                        </a>
                    </div>
                <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php endwhile;

get_footer();