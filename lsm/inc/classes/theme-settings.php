<?php
/**
 * 
 */
class ThemeSettingsClass {
	const SCRIPTS_VERSION    = '1.0.0';

	public function __construct(){
		$this->scriptsDir = get_theme_file_uri().'/assets/js';
        $this->stylesDir = get_theme_file_uri().'/assets/css';

		$this->actions_init();
	}

	public function actions_init(){
		add_action( 'wp_enqueue_scripts', array( $this, 'scripts_styles' ) );
		add_action( 'after_setup_theme', array( $this, 'theme_setup' ) );
		add_action( 'wp_footer',  array( $this, 'js_variables' ) );
		add_action( 'widgets_init', array( $this, 'widgets_init') );
		// add_action( 'init', array( $this, 'custom_posts_type') );
		// add_action( 'init', array( $this, 'custom_taxonomy') );
		
		add_filter( 'upload_mimes', array( $this, 'enable_svg_types'), 99 );
	}

	public function scripts_styles() {
		wp_enqueue_style( 'lsm', $this->stylesDir.'/main.min.css' , '', self::SCRIPTS_VERSION);
    	wp_enqueue_style( 'vizex-style', get_stylesheet_uri() );

    	wp_enqueue_script( 'google-maps-key', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDOqauekT_r4JzPY18FOWN4N8DFR3hXo1U&libraries=places', array( 'jquery' ), self::SCRIPTS_VERSION, true );
    	wp_enqueue_script( 'particles', 'https://cdnjs.cloudflare.com/ajax/libs/particles.js/2.0.0/particles.min.js', array( 'jquery' ), '', true );
    	
    	wp_enqueue_script( 'lsm', $this->scriptsDir.'/all.min.js', array( 'jquery' ), self::SCRIPTS_VERSION, true );
        
    }

    public function theme_setup(){
    	load_theme_textdomain( 'lsm' );
	    add_theme_support( 'automatic-feed-links' );
	    add_theme_support( 'title-tag' );
	    add_theme_support( 'post-thumbnails' );
	    add_theme_support( 'post-formats', array( 'video', 'audio' ) );

	    add_image_size( 'service-thumbnails', 360, 160, true );
	    add_image_size( 'new-thumbnails', 360, 240, true );
	    add_image_size( 'product-thumbnails', 848, 436, true );

	    register_nav_menus( array(
	        'main'          => __( 'Main Menu', 'lsm' )
	    ) );

	    if( function_exists('acf_add_options_page') ) {
		    $general = acf_add_options_page(array(
		        'page_title'    => __('Theme General Settings', 'lsm'),
		        'menu_title'    => __('Theme Settings', 'lsm'),
		        'redirect'      => false,
		        'capability'    => 'edit_posts',
		        'menu_slug'     => 'theme-settings',
		    ));
		}
    }

    public function widgets_init(){
    	register_sidebar( array(
	        'name'          => __( 'Footer 1', 'lsm' ),
	        'id'            => 'footer-1',
	        'description'   => __( 'Add widgets here to appear in your footer.', 'lsm' ),
	        'before_widget' => '<section id="%1$s" class="widget %2$s">',
	        'after_widget'  => '</section>',
	        'before_title'  => '<h5>',
	        'after_title'   => '</h5>',
	    ) );
    }

    public function enable_svg_types($mimes) {
		$mimes['svg'] = 'image/svg+xml';
		return $mimes;
	}

	public function js_variables(){ ?>
		<script type="text/javascript">
	        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>',
	        	asyncUpload = '<?php echo admin_url('async-upload.php'); ?>';
	    </script>
	<?php }

	public function custom_posts_type(){
		$services_labels = array(
			'name'					=> __('Team', 'lsm'),
			'singular_name'			=> __('Team', 'lsm'),
			'add_new'				=> __('Add Person', 'lsm'),
			'add_new_item'			=> __('Add New Person', 'lsm'),
			'edit_item'				=> __('Edit Person', 'lsm'),
			'new_item'				=> __('New Person', 'lsm'),
			'view_item'				=> __('View Person', 'lsm')
		);

		$services_args = array(
			'label'               => __('Team', 'lsm'),
			'description'         => __('Person information page', 'lsm'),
			'labels'              => $services_labels,
			'supports'            => array( 'title', 'thumbnail', 'editor' ),
			'taxonomies'          => array( '' ),
			'hierarchical'        => true,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'has_archive'         => true,
			'can_export'          => true,
			'show_in_nav_menus'   => true,
			'publicly_queryable'  => false,
			'exclude_from_search' => false,
			'query_var'           => true,
			'rewrite'             => array(
				'slug'			  => 'team'
			),
			'capability_type'     => 'post',
			'menu_position'		  => 4,
			'menu_icon'           => 'dashicons-buddicons-buddypress-logo'
		);

		register_post_type( 'team', $services_args );
	}

	public function custom_taxonomy(){
		$taxonomy_labels = array(
			'name'                        => __('Projects categories', 'lsm'),
			'singular_name'               => __('Projects category', 'lsm'),
			'menu_name'                   => __('Projects categories', 'lsm'),
		);

		$taxonomy_rewrite = array(
			'slug'                  => 'projects-categories',
			'with_front'            => true,
			'hierarchical'          => true,
		);

		$taxonomy_args = array(
			'labels'              => $taxonomy_labels,
			'hierarchical'        => true,
			'public'              => true,
			'show_ui'             => true,
			'show_admin_column'   => true,
			'show_in_nav_menus'   => true,
			'show_tagcloud'       => true,
			'rewrite'             => $taxonomy_rewrite,
		);
		register_taxonomy( 'projects-categories', 'project', $taxonomy_args );
	}

    public function __return_false() {
        return false;
    }
}

$ThemeSettingsClass = new ThemeSettingsClass();